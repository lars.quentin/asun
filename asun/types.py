from dataclasses import dataclass
from typing import Dict, List
from uuid import UUID
from asun.utilities import CONST


@dataclass(frozen=True)
class State:
    id: UUID
    typ: str = CONST.EXISTENTIAL

    def __eq__(self, other) -> bool:
        return isinstance(other, State) and self.id == other.id


class Position:
    def __init__(self, state: State, index: int, special: bool = False, visited: bool = False, search_idx: int = -1):
        self.state: State = state
        self.index: int = index
        self.special: bool = special
        self.visited: bool = visited
        self.search_idx: int = search_idx

    def __eq__(self, other) -> bool:
        return isinstance(other, Position) and other.state == self.state and other.index == self.index

    def __hash__(self) -> int:
        return hash(self.state) + self.index

    def __repr__(self) -> str:
        return f'POSITION: {str(self)}'

    def __str__(self) -> str:
        return f'State: {self.state}, Index: {self.index}, Special: {self.special}'


Vertices: List[Position]
Edges: Dict[Position, List[Position]]
Transition_function: Dict[State, Dict[str, List[State]]]
Alphabet: List[str]