import re
from typing import List


class CONST:
    # simple class only for some constants for better readability
    EPSILON: str = '$'
    EXISTENTIAL: str = 'exist'
    UNIVERSAL: str = 'uni'
    NEGATION: str = 'neg'
    EMPTY: str = '?'
    AND: str = '$'
    OR: str = '|'
    CONCAT: str = '+'
    NEG: str = '-'
    LETTER: str = 'L'
    KLEENE: str = '*'

    LEFT: int = 0
    RIGHT: int = 1


prio = {'!': 5, '*': 4, '+': 3, '&': 2, '|': 1}


def is_letter(char: str) -> bool:
    """
    Method for checking if given char is not a special char

    Parameters
    ----------
    char: str:
        character to be checked

    Returns
    -------
    bool:
        true if the char is no special operator, false otherwise

    Raises
    ------
    TypeError
        If given char is not a string

    ValueError
        if length of char is not 1
    """
    if not isinstance(char, str):
        raise TypeError

    if len(char) != 1:
        raise ValueError

    return char not in prio.keys() and char not in ['(', ')']


def add_concat(inp_regex: str) -> str:
    """
    Method takes regular expression and adds necessary concat symbols

    Parameters
    ----------
    inp_regex: str:
        regex for which concat symbols should be added

    Returns
    -------
    str:
        the modified regex

    Raises
    ------
    TypeError
        If given regex is not a string

    ValueError
        if regex is empty
    """
    if not isinstance(inp_regex, str):
        raise TypeError

    if not inp_regex:
        raise ValueError

    res = inp_regex[0]
    for c in inp_regex[1:]:
        # delete all spaces in the original input
        prev_c = res[-1]
        # we need to add concat in the right places so we can build the postfix string and the automaton
        if (prev_c in [')', '*'] or is_letter(prev_c)) and (c == '(' or is_letter(c)):
            res += '+'
        res += c

    return res


def delete_spaces(inp: str) -> str:
    """
    Method deletes all spaced in the given input

    Parameters
    ----------
    inp: str:
        input to be cleared

    Returns
    -------
    str:
        input without spaces

    Raises
    ------
    TypeError
        If given input is not a string
    """
    if not isinstance(inp, str):
        raise TypeError

    return re.sub(r'\s+', '', inp)


def to_lower(inp: str) -> str:
    """
    Method turns given input into lower case

    Parameters
    ----------
    inp: str:
        input to be lowered

    Returns
    -------
    str:
        input but in lower case

    Raises
    ------
    TypeError
        If given input is not a string
    """
    if not isinstance(inp, str):
        raise TypeError

    return inp.lower()


def remove_redundant_eps(inp: str) -> str:
    """
    Method removes redundant epsilons from given input

    Parameters
    ----------
    inp: str:
        input that has to be cleared of unnecessary epsilons

    Returns
    -------
    str:
        cleared input

    Raises
    ------
    TypeError
        If given input is not a string
    """
    if not isinstance(inp, str):
        raise TypeError

    inp = re.sub('[$]+', '$', inp)

    return inp if len(inp) == 1 else re.sub('[$]', '', inp)


def regex_to_postfix(inp_regex: str) -> str:
    """
    Method uses Shunting Yard Algo to transform infix ERE to postfix ERE
    reference: https://blog.cernera.me/converting-regular-expressions-to-postfix-notation-with-the-shunting-yard-algorithm/
    Parameters
    ----------
    inp_regex: str:
        regex that gets converted into postfix notation

    Returns
    -------
    str:
        regex in postfix notation

    Raises
    ------
    TypeError
        If given regex is not a string

    ValueError
        if regex is empty
    """
    if not isinstance(inp_regex, str):
        raise TypeError

    if not inp_regex:
        raise ValueError

    op_stack: List[str] = []
    res = ''
    for c in inp_regex:
        if is_letter(c):
            # if the char is a letter we can directly append it to the output
            res += c
        elif c == '(':
            # here we can just append it to the operator stack
            # we only know that we got another opening bracket and are one level deeper
            op_stack.append(c)
        elif c == ')':
            # we have to append all operators to the output until we find ')'
            # we do this since after this we have processed one pair of parenthesis
            while op_stack[-1] != '(':
                res += op_stack.pop()
            # at the end we need to remove the opening bracket
            op_stack.pop()
        else:
            # here we have an operator as input symbol
            if not op_stack:
                # if we have no operators on our stack we can just append this one
                op_stack.append(c)
            else:
                # as long as we don't have an opening bracket on top of the stack and the priority of the top of the stack
                # is higher than our current operator ...
                while op_stack[-1] != '(' and prio[op_stack[-1]] >= prio[c]:
                    if not op_stack:
                        # if nothing is left we can break
                        break
                    # we add the top of the stack to our output since this one has a higher binding than the current one
                    res += op_stack.pop()
                # At the end we add our current operator to the stack
                op_stack.append(c)

    while op_stack:
        # since it's possible that we have operators left ( for instance: concat at the end of the regex)
        # we need to add all operators from the stack to our output
        res += op_stack.pop()

    return res
