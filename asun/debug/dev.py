from typing import Dict, Any
from asun.automata import CONST
import uuid



class State:
    def __init__(self, state_type: str = CONST.EXISTENTIAL):
        self.id: uuid.UUID = uuid.uuid4()
        self.type: str = state_type

    def __str__(self) -> str:
        return f'ID: {self.id}; Type: {self.type}'

    def __repr__(self) -> str:
        return f'STATE: {str(self)}'

    def __hash__(self) -> int:
        return hash(self.id)

    def __eq__(self, other) -> bool:
        return isinstance(other, State) and self.id == other.id





def verify_automata(self) -> bool:
    # checking Theorem 1:

    # 1) Since the construction doesn't allow more than one init/fin state we don't need to check this
    # but we check that the fin state has no outgoing transitions
    for _, state_list in self.trans_func[self.fin_state].items():
        if len(state_list) != 0:
            return False

    # 2) no transitions in the initial state
    for state in self.states:
        if state != self.init_state:
            for _, state_list in self.trans_func[state].items():
                if self.init_state in state_list:
                    return False

    # we don't need to check 3) since the construction doesn't allows anything other than one-to-one

    # 4) every universal state has exactly 2 \epsilon successors and no other successors
    for state in self.states:
        if state.type == CONST.UNIVERSAL:
            for char, state_list in self.trans_func[state].items():
                if char == CONST.EPSILON and len(state_list) != 2:
                    return False
                elif char != CONST.EPSILON and len(state_list) != 0:
                    return False

    # 5) check conditions for all existential states
    for state in self.states:
        if state.type == CONST.EXISTENTIAL:
            if self.check_succs(state, epsilon=True) and self.check_succs(state, no_succ=True):
                return False
            if self.check_succs(state, epsilon=True) and self.check_succs(state, sigma=True):
                return False
            if self.check_succs(state, no_succ=True) and self.check_succs(state, sigma=True):
                return False

    return True


def check_succs(self, q: State, epsilon: bool = False, no_succ: bool = False, sigma: bool = False) -> bool:
    if epsilon:
        return len(self.trans_func[q][CONST.EPSILON]) in [1, 2]

    if no_succ:
        for _, state_list in self.trans_func[q].items():
            if len(state_list) != 0:
                return False

        return True

    if sigma:
        has_sigma = False
        for char, state_list in self.trans_func[q].items():
            if char != CONST.EPSILON and len(state_list) != 0 and has_sigma:
                return False

            if char != CONST.EPSILON and len(state_list) != 0 and not has_sigma:
                has_sigma = True

        return has_sigma


def process_not(inp_regex: str) -> str:
    res = ''
    idx = 0
    for _ in range(len(inp_regex)):
        if idx >= len(inp_regex):
            break
        if inp_regex[idx] != '!':
            res += inp_regex[idx]
            idx += 1
        else:
            if is_letter(inp_regex[idx + 1]):
                res += (inp_regex[idx + 1] + inp_regex[idx])
                idx += 2
            elif inp_regex[idx + 1] == '(':
                res += process_brackets(inp_regex[idx+1:])
                idx = len(res)

    return res


def process_brackets(inp_regex: str) -> str:
    res = '('
    bracket_cnt = 1
    idx = 1
    for _ in inp_regex[1:]:
        if not bracket_cnt:
            break

        if inp_regex[idx] != '!':
            res += inp_regex[idx]
        else:
            if not is_letter(inp_regex[idx+1]):
                res += process_brackets(inp_regex[idx+1:])
                idx = len(res)
                continue
            else:
                res += (inp_regex[idx+1] + '!')
        if inp_regex[idx] == '(':
            bracket_cnt += 1
        if inp_regex[idx] == ')':
            bracket_cnt -= 1
        idx += 1

    return res + '!'

def concat_nfa(nfa_1, nfa_2) -> Dict[str, Any]:
    res: Dict[str, Any] = dict()
    res['states'] = nfa_1['states'] + nfa_2['states']
    res['init_state'] = nfa_1['init_state']
    res['fin_state'] = nfa_2['fin_state']
    res['Alphabet'] = list(set(nfa_1['Alphabet']) | set(nfa_2['Alphabet']))
    res['trans_func'] = {}

    for state in res['states']:
        if state in nfa_1['states']:
            res['trans_func'][state] = nfa_1['trans_func'][state]
        elif state in nfa_2['states']:
            res['trans_func'][state] = nfa_2['trans_func'][state]

    for state in nfa_1['fin_state']:
        res['trans_func'][state][CONST.EPSILON].append(nfa_2['init_state'])

    return res


def kleene_nfa(nfa) -> Dict[str, Any]:
    res: Dict[str, Any] = dict()
    res['states'] = [uuid.uuid4()] + nfa['states'] + [uuid.uuid4()]
    res['init_state'] = res['states'][0]
    res['fin_state'] = [res['states'][-1]]
    res['Alphabet'] = nfa['Alphabet']
    res['trans_func'] = {}

    for state in res['states']:
        if state in nfa['states']:
            res['trans_func'][state] = nfa['trans_func'][state]
        else:
            res['trans_func'][state] = {}
            for c in res['Alphabet']:
                res['trans_func'][state][c] = []

    res['trans_func'][res['init_state']][CONST.EPSILON].append(nfa['init_state'])

    for state in nfa['fin_state']:
        res['trans_func'][state][CONST.EPSILON].append(nfa['init_state'])
        res['trans_func'][state][CONST.EPSILON].extend(nfa['fin_state'])

    res['trans_func'][res['init_state']][CONST.EPSILON].extend(res['fin_state'])

    return res


def union_nfa(nfa_1, nfa_2) -> Dict[str, Any]:
    res: Dict[str, Any] = dict()

    res['states'] = [uuid.uuid4()] + nfa_1['states'] + nfa_2['states']
    res['init_state'] = res['states'][0]
    res['fin_state'] = nfa_1['fin_state'] + nfa_2['fin_state']
    res['Alphabet'] = list(set(nfa_1['Alphabet']) | set(nfa_2['Alphabet']))
    res['trans_func'] = {}

    for state in res['states']:
        if state in nfa_1['states']:
            res['trans_func'][state] = nfa_1['trans_func'][state]
        elif state in nfa_2['states']:
            res['trans_func'][state] = nfa_2['trans_func'][state]
        else:
            res['trans_func'][state] = {}
            for c in res['Alphabet']:
                res['trans_func'][state][c] = []

    res['trans_func'][res['init_state']][CONST.EPSILON].extend([nfa_1['init_state'], nfa_2['init_state']])

    return res



 for k, v in res.letter_cnt.items():
            components = res.letter_cnt[k].split('+')
            tmp = sum([int(i) for i in components if i.isnumeric()])
            res.letter_cnt[k] = "+".join([i for i in components if not i.isnumeric()])

            res.letter_cnt[k] += ("+" + str(tmp))
            res.letter_cnt[k] = res.letter_cnt[k][1::]


 for k, v in chain(self.letter_cnt.items(), asun.letter_cnt.items()):
            if k not in res.letter_cnt.keys():
                res.letter_cnt[k] = v
            else:
                res.letter_cnt[k] += ("+" + v)


class Equation:
    static_variable_cnt = 0

    def __init__(self, start: int = 0, kleene: bool = False, multi: List[int] = None):
        self.start: int = start
        self.kleene: bool = kleene
        self.multi: List[int] = multi

    def __add__(self, other: Equation):
        if other.kleene:
            self.kleene = True

        self.start += other.start

        for x in other.multi:
            self.multi.append(x)

        return Equation(self.start, self.kleene, self.multi)

    def star(self):
        if not self.kleene:
            self.kleene = True
        if not self.multi:
            self.start = 0
            self.multi.append(self.start)
            return