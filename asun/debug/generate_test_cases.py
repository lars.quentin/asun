import random
import re
import string
import uuid
from dataclasses import dataclass

from asun.automata import CONST


@dataclass(frozen=True)
class State:
    id: uuid.UUID
    typ: str = CONST.EXISTENTIAL

    def __eq__(self, other) -> bool:
        return isinstance(other, State) and self.id == other.id


@dataclass(frozen=True)
class Position:
    state: State
    index: int
    special: bool = False

    def __eq__(self, other) -> bool:
        return isinstance(other, Position) and other.state == self.state and other.index == self.index

    def __hash__(self) -> int:
        return hash(self.state) + self.index


class SearchPosition:
    def __init__(self, p: Position, visited: bool = False, index: int = -1):
        self.position = p
        self.visited = visited
        self.index = index


def gen_random_a(size: int, char: str) -> str:
    n = random.random()*size+1
    n = int(n)
    return ''.join([char for _ in range(n)])


def gen_random_seq(size: int) -> str:
    n = random.random() * size + 1
    n = int(n)
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(n))


def main():
    s1 = State(uuid.uuid4())
    s2 = State(uuid.uuid4())
    print(s1)
    print(hash(s1))
    print(s2)
    print(s1 == s2)
    # for _ in range(10):
    #    # test = ''.join(random.choice(string.ascii_lowercase) for _ in range(6))
    #    test = gen_random_seq(20)
    #    test += 'c'
    #    res = bool(re.search('^(aa+|bb+)c$', test))
    #    print(f',(\'((aaa*) | (bbb*))c\', \'{test}\', {res})')


if __name__ == '__main__':
    main()
